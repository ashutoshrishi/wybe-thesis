(TeX-add-style-hook
 "refs"
 (lambda ()
   (LaTeX-add-bibitems
    "lpvm2015"
    "alpernSSA"
    "cytronSSA"
    "gsa"
    "ssi"
    "appelfp"
    "cpp_compiler"
    "Lattner:MSThesis02"
    "irnote"
    "make"
    "tichy_recomp_context"
    "smarter_recompilation"
    "cost_of_recompilation"
    "boehmgc"
    "mir"
    "sil"))
 :bibtex)

